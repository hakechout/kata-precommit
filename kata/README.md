[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/hakechout%2Fkata-precommit/master)

# Kata Pre-commit

Veuillez regarder les fichiers dans src/

Vous pouvez executer le code :
- dans le terminal "python src/main.py" (sans viz)
- ou dans un jupyter notebook "%run src/main.py" (avec viz)
